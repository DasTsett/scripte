#region Prelude
using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game.ModAPI.Ingame.Utilities;

// Change this namespace for each script you create.
namespace SpaceEngineers.UWBlockPrograms.Raupe {
    public sealed class Program : MyGridProgram {
    // Your code goes between the next #endregion and #region
#endregion
// Raupi v1.0
MyIni _ini = new MyIni();
MyCommandLine _commandLine = new MyCommandLine();

IEnumerator<State> stateMachine = null;
State currentState;
bool crawlForward = false;

public enum State {
    Finish = -1,
    Continue = 0,
    AttachFront = 1,
    AttachBack = 2,
    DetachFront = 3,
    DetachBack = 4,
    Extend = 5,
    Retract = 6
}

IMyPistonBase frontPistonMerge;
IMyPistonBase frontPistonConnector;
IMyShipConnector frontConnector;
IMyShipMergeBlock frontMerger;
IMyPistonBase backPistonMerge;
IMyPistonBase backPistonConnector;
IMyShipConnector backConnector;
IMyShipMergeBlock backMerger;
IMyPistonBase pistonExtender;
float pistonExtenderExtendSpeed = 0.5f;
float pistonExtenderRetractSpeed = 0.5f;

const float pistonExtended = 0.92f;

public void writeTemplate() {
    _ini.Clear();
    _ini.Set("front", "mergePiston", "FrontPistonName");
    _ini.Set("front", "merge", "FrontMergeName");
    _ini.Set("front", "connectorPiston", "FrontConnectorName");
    _ini.Set("front", "connector", "FrontConnectorName");
    _ini.Set("back", "mergePiston", "BackPistonName");
    _ini.Set("back", "merge", "BackMergeName");
    _ini.Set("back", "connectorPiston", "BackConnectorName");
    _ini.Set("back", "connector", "BackConnectorName");
    _ini.Set("extender", "piston", "ExtenderPistonName");
    _ini.Set("extender", "extendSpeed", "ExtendSpeed");
    _ini.Set("extender", "retractSpeed", "RetractSpeed");
    Me.CustomData = _ini.ToString();
}

public void readConfig() {
    MyIniParseResult result;
    if (!_ini.TryParse(Me.CustomData, out result)) 
        throw new Exception(result.ToString());

    frontPistonMerge = (IMyPistonBase) GridTerminalSystem.GetBlockWithName(_ini.Get("front", "mergePiston").ToString());
    if (frontPistonMerge == null) {
        Echo("no frontPistonMerge");
    }
    frontMerger = (IMyShipMergeBlock) GridTerminalSystem.GetBlockWithName(_ini.Get("front", "merge").ToString());
    if (frontMerger == null) {
        Echo("no frontMerger");
    }
    frontPistonConnector = (IMyPistonBase) GridTerminalSystem.GetBlockWithName(_ini.Get("front", "connectorPiston").ToString());
    if (frontPistonConnector == null) {
        Echo("no frontPistonConnector");
    }
    frontConnector = (IMyShipConnector) GridTerminalSystem.GetBlockWithName(_ini.Get("front", "connector").ToString());
    if (frontConnector == null) {
        Echo("no frontConnector");
    }
    backPistonMerge = (IMyPistonBase) GridTerminalSystem.GetBlockWithName(_ini.Get("back", "mergePiston").ToString());
    if (backPistonMerge == null) {
        Echo("no backPistonMerge");
    }
    backMerger = (IMyShipMergeBlock) GridTerminalSystem.GetBlockWithName(_ini.Get("back", "merge").ToString());
    if (backMerger == null) {
        Echo("no backMerger");
    }
    backPistonConnector = (IMyPistonBase) GridTerminalSystem.GetBlockWithName(_ini.Get("back", "connectorPiston").ToString());
    if (backPistonConnector == null) {
        Echo("no backPistonConnector");
    }
    backConnector = (IMyShipConnector) GridTerminalSystem.GetBlockWithName(_ini.Get("back", "connector").ToString());
    if (backConnector == null) {
        Echo("no backConnector");
    }
    pistonExtender = (IMyPistonBase) GridTerminalSystem.GetBlockWithName(_ini.Get("extender", "piston").ToString());
    if (pistonExtender == null) {
        Echo("no pistonExtender");
    }
    pistonExtenderExtendSpeed = _ini.Get("extender", "extendSpeed").ToSingle();
    pistonExtenderRetractSpeed = _ini.Get("extender", "retractSpeed").ToSingle();
}

public IEnumerator<State> attachFront() {
    
    frontPistonMerge.Extend();
    frontMerger.Enabled = true;
    while(frontPistonMerge.Status == PistonStatus.Extending && (frontPistonMerge.CurrentPosition / frontPistonMerge.MaxLimit) <= pistonExtended) {
        Echo("attachFront: Connecting Front Merger");
        yield return State.Continue;
    }
    if(!frontMerger.IsConnected) {
        stateMachine.Dispose();
        throw new Exception("front Mergeblock not attached -> Abort");
    }

    frontPistonConnector.Extend();
    while(frontPistonConnector.Status == PistonStatus.Extending) {
        Echo("attachFront: Extending Front Piston Connector");
        yield return State.Continue;
    }
    while(frontConnector.Status != MyShipConnectorStatus.Connectable) {
        Echo("attachFront: Connecting Front Connector");
        yield return State.Continue;
    }
    frontConnector.Connect();

    if(frontConnector.Status != MyShipConnectorStatus.Connected) {
        stateMachine.Dispose();
        throw new Exception("front Connector not attached -> Abort");
    }
    if (crawlForward) {
        yield return State.DetachBack;
    } else {
        yield return State.Finish;
    }
}

public IEnumerator<State> detachFront() {
    if (!backMerger.IsConnected) {
        throw new Exception("Raupi don't want to Fly (detachFront)");
    }
    frontConnector.Disconnect();
    while(frontConnector.Status == MyShipConnectorStatus.Connected) {
        Echo("detachFront: frontConnector disconnect");
        yield return State.Continue;
    }
    frontPistonConnector.Retract();
    frontMerger.Enabled = false;
    frontPistonMerge.Retract();
    
    while(frontPistonMerge.Status == PistonStatus.Retracting) {
        Echo("detachFront: frontPistonMerge Retracting");
        yield return State.Continue;
    }
    while(frontPistonConnector.Status == PistonStatus.Retracting) {
        Echo("detachFront: frontPistonConnector Retracting");
        yield return State.Continue;
    }
    if (crawlForward) {
        yield return State.Extend;
    } else {
        yield return State.Finish;
    }
}

public IEnumerator<State> attachBack() {
    
    backPistonMerge.Extend();
    backMerger.Enabled = true;
    while(backPistonMerge.Status == PistonStatus.Extending && (backPistonMerge.CurrentPosition / backPistonMerge.MaxLimit) <= pistonExtended) {
        Echo("attachBack: Connecting Back Merger");
        yield return State.Continue;
    }
    if(!backMerger.IsConnected) {
        stateMachine.Dispose();
        throw new Exception("back Mergeblock not attached -> Abort");
    }
    backPistonConnector.Extend();
    while(backPistonConnector.Status == PistonStatus.Extending && (backPistonConnector.CurrentPosition / backPistonConnector.MaxLimit) <= pistonExtended) {
        Echo("attachBack: Extending Back Connector");
        yield return State.Continue;
    }
    while(backConnector.Status != MyShipConnectorStatus.Connectable) {
        Echo("attachBack: Waiting for Back Connector");
        yield return State.Continue;
    }
    backConnector.Connect();
    
    if(backConnector.Status != MyShipConnectorStatus.Connected) {
        stateMachine.Dispose();
        throw new Exception("back Connector not attached -> Abort");
    }
    if (crawlForward) {
        yield return State.DetachFront;
    } else {
        yield return State.Finish;
    }
}

public IEnumerator<State> detachBack() {
    if (!frontMerger.IsConnected) {
        throw new Exception("Raupi don't want to Fly (detachBack)");
    }
    backConnector.Disconnect();
    while(backConnector.Status == MyShipConnectorStatus.Connected) {
        Echo("detachBack: backConnector disconnect");
        yield return State.Continue;
    }
    backPistonConnector.Retract();
    backMerger.Enabled = false;
    backPistonMerge.Retract();
    
    while(backPistonMerge.Status == PistonStatus.Retracting) {
        Echo("detachBack: backPistonMerge Retracting");
        yield return State.Continue;
    }
    while(backPistonConnector.Status == PistonStatus.Retracting) {
        Echo("detachBack: backPistonConnector Retracting");
        yield return State.Continue;
    }
    if (crawlForward) {
        yield return State.Retract;
    } else {
        yield return State.Finish;
    }
}

public IEnumerator<State> extend() {
    //pistonExtender.Extend();
    pistonExtender.Velocity = pistonExtenderExtendSpeed;
    while(pistonExtender.Status == PistonStatus.Extending) {
        Echo($"Extending: {pistonExtender.Velocity} / {pistonExtender.MaxVelocity}");
        yield return State.Continue;
    }
    if (crawlForward) {
        yield return State.AttachFront;
    } else {
        yield return State.Finish;
    }
}

public IEnumerator<State> retract() {
    //pistonExtender.Retract();
    if (frontPistonMerge.CurrentPosition / frontPistonMerge.MaxLimit <= 0.97f) {
        frontConnector.Disconnect();
        while(frontPistonMerge.Status == PistonStatus.Extending) {
            Echo($"retract: fixHeight {frontPistonMerge.CurrentPosition} / {frontPistonMerge.MaxLimit}");
            yield return State.Continue;
        }
        frontConnector.Connect();
    }
    pistonExtender.Velocity = -pistonExtenderRetractSpeed;
    while(pistonExtender.Status == PistonStatus.Retracting) {
        Echo($"retract: {pistonExtender.Velocity} / {pistonExtender.MaxVelocity}");
        yield return State.Continue;
    }
    
    if (crawlForward) {
        yield return State.AttachBack;
    } else {
        yield return State.Finish;
    }
}

public IEnumerator<State> fixHeight() {
    frontMerger.Enabled = true;
    backMerger.Enabled = true;
    frontPistonMerge.Extend();
    backPistonMerge.Extend();

    frontConnector.Disconnect();
    backConnector.Disconnect();
    frontPistonConnector.Retract();
    backPistonConnector.Retract();
    
    while(!backMerger.IsConnected) {
        Echo("Check BackMerger isConnected");
        yield return State.Continue;
    }
    while(!frontMerger.IsConnected) {
        Echo("Check FrontMerger isConnected");
        yield return State.Continue;
    }
    // 240 Tick sollten ca 4 Sekunden sein und bei einer Geschwindigkeit von 0.5m/s ist das ein Block
    for(int i = 0; i < 240; ++i) {
        Echo("Safty Waiting");
        yield return State.Continue;
    }

    frontMerger.Enabled = false;
    float lastPistonPostion = 0f;
    // Bewegungspruefung falls der andere piston Blockiert
    while(backPistonMerge.Status == PistonStatus.Extending && Math.Abs(backPistonMerge.CurrentPosition - lastPistonPostion) > 0.001f) {
        Echo("Extending BackMerger");
        lastPistonPostion = backPistonMerge.CurrentPosition;
        yield return State.Continue;
    }
    frontMerger.Enabled = true;
    // 240 Tick sollten ca 4 Sekunden sein und bei einer Geschwindigkeit von 0.5m/s ist das ein Block
    for(int i = 0; i < 240; ++i) {
        Echo("Safty Waiting");
        yield return State.Continue;
    }
    backMerger.Enabled = false;
    lastPistonPostion = 0f;
    // spaetestens hier sollten sich die Pistons extended haben
    while(frontPistonMerge.Status == PistonStatus.Extending ) {
        Echo("Extending FrontMerger");
        lastPistonPostion = frontPistonMerge.CurrentPosition;
        yield return State.Continue;
    }
    backMerger.Enabled = true;

    while(frontPistonMerge.Status == PistonStatus.Extending && Math.Abs(frontPistonMerge.CurrentPosition - frontPistonMerge.MaxLimit) > 0.001f) {
        Echo("Check frontPistonMerge Extending");
        yield return State.Continue;
    }
    while(backPistonMerge.Status == PistonStatus.Extending && Math.Abs(backPistonMerge.CurrentPosition - backPistonMerge.MaxLimit) > 0.001f) {
        Echo("Check backPistonMerge Extending");
        yield return State.Continue;
    }

    frontPistonConnector.Extend();
    backPistonConnector.Extend();
    while(frontPistonConnector.Status == PistonStatus.Extending) {
        Echo("Check frontPistonConnector Extending");
        yield return State.Continue;
    }
    while(backPistonConnector.Status == PistonStatus.Extending) {
        Echo("Check backPistonConnector Extending");
        yield return State.Continue;
    }
    frontConnector.Connect();
    backConnector.Connect();
    if (frontConnector.Status != MyShipConnectorStatus.Connected || backConnector.Status != MyShipConnectorStatus.Connected) {
        throw new Exception("Heightfix failed");
    }
    // Wenn fertig soll sicherheits halber der Hintere angestoßen werden
    yield return State.AttachBack;
}

public void copyConfigToStorage() {
    MyIniParseResult result;
    if (!_ini.TryParse(Me.CustomData, out result)) 
        throw new Exception(result.ToString());
    Storage = _ini.ToString();
    readConfig();
}

public Program() {

    // try {
        readConfig();
    // } catch (Exception e) {
    //     copyConfigToStorage();
    // }

}
public void Save() {
    _ini.Set("state", "id", $"{currentState}");
    Storage= _ini.ToString();
}

public void Main(string argument, UpdateType updateSource) {
    if ((updateSource & UpdateType.Once) == UpdateType.Once) {
        if (stateMachine != null) {
            bool moreSteps = stateMachine.MoveNext();
            if (moreSteps) {
                State nextStep = stateMachine.Current;
                if (nextStep == State.Finish) {
                    Echo("Finish");
                    stateMachine.Dispose();
                    stateMachine = null;
                    crawlForward = false;
                } else {
                    if (nextStep != State.Continue) {
                        stateMachine.Dispose();
                        switch (nextStep) {
                            case State.AttachFront:
                                stateMachine = attachFront();
                                break;
                            case State.AttachBack:
                                stateMachine = attachBack();
                                break;
                            case State.DetachFront:
                                stateMachine = detachFront();
                                break;
                            case State.DetachBack:
                                stateMachine = detachBack();
                                break;
                            case State.Extend:
                                stateMachine = extend();
                                break;
                            case State.Retract:
                                stateMachine = retract();
                                break;
                        }
                    }
                    Echo("Continue");
                    Runtime.UpdateFrequency |= UpdateFrequency.Once;
                } 
            }
        }
    } else {
        if (_commandLine.TryParse(argument)) {
            if(_commandLine.Switch("writeTemplate")) {
                writeTemplate();
            } else if (_commandLine.Switch("copyConfigToStorage")) {
                copyConfigToStorage();
                Echo(Storage);
            } else if (_commandLine.Switch("echoStorage")) {
                Echo(Storage);
            } else if (_commandLine.Switch("Save")) {
                Save();
            } else if (_commandLine.Switch("attachFront")) {
                if (stateMachine != null) {
                    stateMachine.Dispose();
                }
                stateMachine = attachFront();
                Runtime.UpdateFrequency |= UpdateFrequency.Once;
            } else if (_commandLine.Switch("detachFront")) {
                if (stateMachine != null) {
                    stateMachine.Dispose();
                }
                stateMachine = detachFront();
                Runtime.UpdateFrequency |= UpdateFrequency.Once;
            } else if (_commandLine.Switch("attachBack")) {
                if (stateMachine != null) {
                    stateMachine.Dispose();
                }
                stateMachine = attachBack();
                Runtime.UpdateFrequency |= UpdateFrequency.Once;
            } else if (_commandLine.Switch("detachBack")) {
                if (stateMachine != null) {
                    stateMachine.Dispose();
                }
                stateMachine = detachBack();
                Runtime.UpdateFrequency |= UpdateFrequency.Once;
            } else if (_commandLine.Switch("extend")) {
                if (stateMachine != null) {
                    stateMachine.Dispose();
                }
                stateMachine = extend();
                Runtime.UpdateFrequency |= UpdateFrequency.Once;
            } else if (_commandLine.Switch("retract")) {
                if (stateMachine != null) {
                    stateMachine.Dispose();
                }
                stateMachine = retract();
                Runtime.UpdateFrequency |= UpdateFrequency.Once;
            } else if (_commandLine.Switch("crawl")) {
                crawlForward = true;
                if (stateMachine == null) {
                    bool frontMergerEnabled = frontMerger.Enabled;
                    bool backMergerEnabled = backMerger.Enabled;
                    bool extended = pistonExtender.Status == PistonStatus.Extended;
                    if (frontMergerEnabled && backMergerEnabled && extended) {
                        stateMachine = detachBack();
                    } else if (frontMergerEnabled && backMergerEnabled && !extended) {
                        stateMachine = detachFront();
                    } else if (frontMergerEnabled && !backMergerEnabled && extended) {
                        stateMachine = retract();
                    } else if (frontMergerEnabled && !backMergerEnabled && !extended) {
                        stateMachine = attachBack();
                    } else if (!frontMergerEnabled && backMergerEnabled && extended) {
                        stateMachine = attachFront();
                    } else if (!frontMergerEnabled && backMergerEnabled && !extended) {
                        stateMachine = extend();
                    } else if (!frontMergerEnabled && !backMergerEnabled) {
                        throw new Exception("Super Raupe ist am fliegen");
                    } 
                }
                Runtime.UpdateFrequency |= UpdateFrequency.Once;
            } else if (_commandLine.Switch("finish")) {
                crawlForward = false;
            } else if (_commandLine.Switch("kill")) {
                crawlForward = false;
                stateMachine.Dispose();
                stateMachine = null;
            // } else if (_commandLine.Switch("fixHeight")) {
            //     crawlForward = false;
            //     stateMachine = fixHeight();
            //     Runtime.UpdateFrequency |= UpdateFrequency.Once;
            }  
        }
    }
}



#region PreludeFooter
    }
}
#endregion


