#region Prelude
using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;

// Change this namespace for each script you create.
namespace SpaceEngineers.UWBlockPrograms.Cargo.StopWhenFull {
    public sealed class Program : MyGridProgram {
    // Your code goes between the next #endregion and #region
#endregion

const int idKeyboard = 1;
const int idDisplay = 0;
List<IMyCargoContainer> blocks = new List<IMyCargoContainer>();
IMyProgrammableBlock progBlock;

public void appendTo(int id, String text) {
    // Display hat 11 Zeilen in Defaultfont
    IMyTextSurface currSurface = progBlock.GetSurface(id);
    currSurface.ContentType = VRage.Game.GUI.TextPanel.ContentType.TEXT_AND_IMAGE;
    currSurface.WriteText(currSurface.GetText() + "\n" + text);
}

public void writeTo(int id, String text) {
    IMyTextSurface currSurface = progBlock.GetSurface(id);
    currSurface.ContentType = VRage.Game.GUI.TextPanel.ContentType.TEXT_AND_IMAGE;
    currSurface.WriteText(text);
}

public Program() {
    
}
public void Save() {

}

public void Main(string argument, UpdateType updateSource) {
    GridTerminalSystem.GetBlocksOfType(blocks);
    progBlock = (IMyProgrammableBlock) GridTerminalSystem.GetBlockWithName(Me.CustomName);
    
    writeTo(idDisplay, "Display");
    writeTo(idKeyboard, "Keyboard");
    appendTo(idDisplay, "appended");

    
    Echo("Count: " + blocks.Count);
    for (int i = 0 ; i < blocks.Count; ++i) {
        IMyCargoContainer current = blocks[i];
        IMyInventory curInv = current.GetInventory();
        Echo(current.DisplayNameText + ": " + curInv.IsFull + " # " + curInv.CurrentVolume + "/" + curInv.MaxVolume);
    }
}

#region PreludeFooter
    }
}
#endregion