#region Prelude
using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game.ModAPI.Ingame.Utilities;

// Change this namespace for each script you create.
namespace SpaceEngineers.UWBlockPrograms.Cargo.DockControl {
    public sealed class Program : MyGridProgram {
    // Your code goes between the next #endregion and #region
#endregion

class DockConfig {
    public IMyShipConnector connector;
    public IMyPistonBase piston;
    public long owner;
    public String currentTask;
}


const String messageTag =  "DockControl";

const String scriptTypeController = "DCController";
const String scriptTypeClient = "DCClient";

const String configSectionGeneral = "General";
const String configNameType = "scriptType";
const String configNameConnectors = "connectors";
const String configNamePiston = "piston";
const String configNameOwner = "owner";

const String programRequest = "request";
const String programRequestDock = "dock";
const String programRequestDockAbort = "dockAbort";
const String programRequestUndock = "undock";
const String programRequestUndockAbort = "undockAbort";

const int idKeyboard = 1;
const int idDisplay = 0;
MyIni _ini = new MyIni();
MyCommandLine _commandLine = new MyCommandLine();
IMyProgrammableBlock progBlock;

String currentType;
IMyBroadcastListener bCastListener;

List<DockConfig> docks = new List<DockConfig>();
List<DockConfig> tasks = new List<DockConfig>();



public void writeTemplate() {
    _ini.Clear();
    _ini.Set(configSectionGeneral, configNameType, scriptTypeController);
    Me.CustomData = _ini.ToString();
}

public void appendTo(int id, String text) {
    // Display hat 11 Zeilen in Defaultfont
    IMyTextSurface currSurface = progBlock.GetSurface(id);
    currSurface.ContentType = VRage.Game.GUI.TextPanel.ContentType.TEXT_AND_IMAGE;
    currSurface.WriteText(currSurface.GetText() + "\n" + text);
}

public void writeTo(int id, String text) {
    IMyTextSurface currSurface = progBlock.GetSurface(id);
    currSurface.ContentType = VRage.Game.GUI.TextPanel.ContentType.TEXT_AND_IMAGE;
    currSurface.WriteText(text);
}

public Program() {
    progBlock = (IMyProgrammableBlock) GridTerminalSystem.GetBlockWithName(Me.CustomName);
    MyIniParseResult result;
    if (!_ini.TryParse(Me.CustomData, out result)) 
        throw new Exception(result.ToString());
    MyIni storageIni = null;
    if (Storage != null) {
        storageIni = new MyIni();
        MyIniParseResult storageResult;
        if (!storageIni.TryParse(Storage, out storageResult)) 
            throw new Exception(storageResult.ToString());
    }

    currentType = _ini.Get(configSectionGeneral, configNameType).ToString();
    if (currentType.Equals(scriptTypeController)) {
        // hier muss noch der aktuelle status geladen werden
        List<String> sectionList = new List<String>();
        _ini.GetSections(sectionList);
        sectionList.Remove(configSectionGeneral);
        foreach (String name in sectionList) {
            DockConfig dock = new DockConfig();
            dock.connector = (IMyShipConnector) GridTerminalSystem.GetBlockWithName(name);
            if (dock.connector != null) {
                dock.piston = (IMyPistonBase) GridTerminalSystem.GetBlockWithName(_ini.Get(name, configNamePiston).ToString());
                dock.owner = -1;
                if (storageIni != null && storageIni.ContainsKey(name, configNameOwner)) {
                    dock.owner = storageIni.Get(name, configNameOwner).ToInt64();
                }
                docks.Add(dock);
            }
        }
        Storage = null;
        bCastListener = IGC.RegisterBroadcastListener(messageTag);
        bCastListener.SetMessageCallback(messageTag);
    } else if (currentType.Equals(scriptTypeClient)) {
        IGC.UnicastListener.SetMessageCallback(messageTag);
    }
}
public void Save() {
    MyIni saveIni = new MyIni();
    foreach (DockConfig dock in docks) {
        saveIni.Set(dock.connector.DisplayNameText, configNameOwner, dock.owner);
    }
    Storage = saveIni.ToString();
}

DockConfig findFreeDock() {
    DockConfig result = null;
    foreach (DockConfig dock in docks)
    {
        if (dock.connector.Status == MyShipConnectorStatus.Unconnected && dock.owner == -1) {
            result = dock;
            break;
        }
    }
    return result;
}

DockConfig findDockOfOwner(long owner) {
    DockConfig result = null;
    foreach (DockConfig dock in docks)
    {
        if (dock.owner == owner) {
            result = dock;
            break;
        }
    }
    return result;
}

void switchShipToDocked(IMyShipConnector dockConnector) {
    
    IMyCubeGrid shipGrid = dockConnector.OtherConnector.CubeGrid;
    List<IMyBatteryBlock> batteries = new List<IMyBatteryBlock>();
    List<IMyLightingBlock> lights = new List<IMyLightingBlock>();
    List<IMyThrust> thruster = new List<IMyThrust>();
    GridTerminalSystem.GetBlocksOfType(batteries, block => block.CubeGrid.Equals(shipGrid));
    GridTerminalSystem.GetBlocksOfType(lights, block => block.CubeGrid.Equals(shipGrid));
    GridTerminalSystem.GetBlocksOfType(thruster, block => block.CubeGrid.Equals(shipGrid));
    foreach (IMyLightingBlock light in lights) {
        light.Enabled = false;
    }
    foreach (IMyBatteryBlock batterie in batteries) {
        batterie.ChargeMode = ChargeMode.Recharge;
    }
    foreach (IMyThrust thrust in thruster) {
        thrust.Enabled = false;
    }
}

void switchShipToUndocked(IMyShipConnector dockConnector) {
    IMyCubeGrid shipGrid = dockConnector.OtherConnector.CubeGrid;
    List<IMyBatteryBlock> batteries = new List<IMyBatteryBlock>();
    List<IMyLightingBlock> lights = new List<IMyLightingBlock>();
    List<IMyThrust> thruster = new List<IMyThrust>();
    GridTerminalSystem.GetBlocksOfType(batteries, block => block.CubeGrid.Equals(shipGrid));
    GridTerminalSystem.GetBlocksOfType(lights, block => block.CubeGrid.Equals(shipGrid));
    GridTerminalSystem.GetBlocksOfType(thruster, block => block.CubeGrid.Equals(shipGrid));
    foreach (IMyLightingBlock light in lights) {
        light.Enabled = true;
    }
    foreach (IMyBatteryBlock batterie in batteries) {
        batterie.ChargeMode = ChargeMode.Auto;
    }
    foreach (IMyThrust thrust in thruster) {
        thrust.Enabled = true;
    }
}

public void Main(string argument, UpdateType updateSource) {

    writeTo(idDisplay, "I am " + currentType);

    if (currentType.Equals(scriptTypeController) ) {
        if (updateSource == UpdateType.Terminal) {
            if (_commandLine.TryParse(argument)) {
                if(_commandLine.Switch("save")) {
                    Save();
                } 
            } else {
                // MyIni saveIni = new MyIni();
                // foreach (DockConfig dock in docks) {
                //     saveIni.Set(dock.connector.DisplayNameText, configNameOwner, dock.owner);
                // }
                // appendTo(idDisplay, saveIni.ToString());
                foreach (DockConfig dock in docks) {
                    appendTo(idDisplay, dock.connector.DisplayNameText + " locked: " + dock.connector.Status);
                    appendTo(idDisplay,  "owner: " + dock.owner);
                    appendTo(idDisplay,  "task: " + dock.currentTask);
                }
            }
        } else if (updateSource == UpdateType.IGC) {
            MyIGCMessage message = bCastListener.AcceptMessage();
            MyTuple<String, String> tuple = message.As<MyTuple<String,String>>();
            String program = tuple.Item1;
            String function = tuple.Item2;
            if (program.Equals(programRequest)) {
                if (function.Equals(programRequestDock)) {
                    DockConfig targetDock = findFreeDock();
                    if (targetDock != null) {
                        targetDock.connector.ShowOnHUD = true;
                        targetDock.piston.Extend();
                        targetDock.owner = message.Source;
                        targetDock.currentTask = function;
                        tasks.Add(targetDock);
                        Runtime.UpdateFrequency |= UpdateFrequency.Update10;
                    }
                } else if (function.Equals(programRequestDockAbort)) {
                    DockConfig targetDock = findDockOfOwner(message.Source);
                    targetDock.piston.Retract();
                    if (targetDock != null) {
                        targetDock.connector.ShowOnHUD = false;
                        targetDock.owner = -1;
                        targetDock.currentTask = null;
                    }
                } else if (function.Equals(programRequestUndock)) {
                    DockConfig targetDock = findDockOfOwner(message.Source);
                    if (targetDock != null) {
                        targetDock.piston.Extend();
                        targetDock.currentTask = function;
                        switchShipToUndocked(targetDock.connector);
                        tasks.Add(targetDock);
                        Runtime.UpdateFrequency |= UpdateFrequency.Update10;
                    }
                } else if (function.Equals(programRequestUndockAbort)) {
                    DockConfig targetDock = findDockOfOwner(message.Source);
                    if (targetDock != null) {
                        targetDock.piston.Retract();
                        targetDock.currentTask = null;
                    }
                }
            }
        } else if (updateSource == UpdateType.Update10) {
            foreach (DockConfig dock in tasks)
            {
                switch (dock.currentTask)
                {
                    case programRequestDock:
                    if (dock.connector.Status == MyShipConnectorStatus.Connected) {
                        switchShipToDocked(dock.connector);
                        dock.currentTask = null;
                        dock.piston.Retract();
                        dock.connector.ShowOnHUD = false;
                    }
                    break;
                    case programRequestUndock:
                    if (dock.connector.Status != MyShipConnectorStatus.Connected) {
                        dock.currentTask = null;
                        dock.owner = -1;
                        dock.piston.Retract();
                    }
                    break;
                }
            }
            tasks.RemoveAll(dock => dock.currentTask == null);
            if (tasks.Count == 0) {
                Runtime.UpdateFrequency &= ~UpdateFrequency.Update10;
            }
        }
    } else if (currentType.Equals(scriptTypeClient)) {
        if (updateSource == UpdateType.Terminal || updateSource == UpdateType.Trigger) {
            if (_commandLine.TryParse(argument)) {
                if(_commandLine.Switch("requestDock")) {
                    IGC.SendBroadcastMessage(messageTag, new MyTuple<String, String>(programRequest, programRequestDock));
                } else if(_commandLine.Switch("requestDockAbort")) {
                    IGC.SendBroadcastMessage(messageTag, new MyTuple<String, String>(programRequest, programRequestDockAbort));
                } else if(_commandLine.Switch("requestUndock")) {
                    IGC.SendBroadcastMessage(messageTag, new MyTuple<String, String>(programRequest, programRequestUndock));
                } else if(_commandLine.Switch("requestUndockAbort")) {
                    IGC.SendBroadcastMessage(messageTag, new MyTuple<String, String>(programRequest, programRequestUndockAbort));
                }
            }
        }
    } 
}

#region PreludeFooter
    }
}
#endregion


