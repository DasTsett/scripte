#region Prelude
using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game.ModAPI.Ingame.Utilities;

// Change this namespace for each script you create.
namespace SpaceEngineers.UWBlockPrograms.MiningArm {
    public sealed class Program : MyGridProgram {
    // Your code goes between the next #endregion and #region
#endregion
class RotorConfig {
    public IMyMotorAdvancedStator rotor;
    public float min;
    public float max;
    public float parking;
}
MyIni _ini = new MyIni();
MyCommandLine _commandLine = new MyCommandLine();
RotorConfig _rotorTop;
RotorConfig _rotorSide;
IMyTextSurface _outputPanelDisplay;
IMyTextSurface _outputPanelKeyboard;

void appendToDisplay(String text) {
    // Display hat 11 Zeilen in Defaultfont   
    _outputPanelDisplay.WriteText(text + "\n", true);
}

void writeToDisplay(String text) {
    _outputPanelDisplay.WriteText(text);
}

void showOffline() {
    _outputPanelKeyboard.ClearImagesFromSelection();
    _outputPanelKeyboard.AddImageToSelection("Cross");
}
void showOnline() {
    _outputPanelKeyboard.ClearImagesFromSelection();
    _outputPanelKeyboard.AddImageToSelection("Arrow");
}

RotorConfig initRotor(string rotorId) {
    RotorConfig result = new RotorConfig();
    String rotorName = _ini.Get(rotorId, "Name").ToString();
    
    result.rotor = (IMyMotorAdvancedStator) GridTerminalSystem.GetBlockWithName(rotorName);
    if (result.rotor == null) {
        appendToDisplay("Rotor \"" + rotorName + "\" not found");
        return null;
    }
    result.min = _ini.Get(rotorId, "Min").ToSingle();
    result.max = _ini.Get(rotorId, "Max").ToSingle();
    result.max = _ini.Get(rotorId, "Max").ToSingle();
    result.parking = _ini.Get(rotorId, "Parking").ToSingle();
    return result;
}

void initParkingPosition(RotorConfig config) {
    appendToDisplay("Move to Parking Position");
    IMyMotorAdvancedStator rotor = config.rotor;
    rotor.RotorLock = true;
    if (config.min <= rotor.LowerLimitDeg || rotor.LowerLimitDeg == float.MinValue) {
        appendToDisplay("By Lower Limit");
        rotor.LowerLimitDeg = config.parking;
        if (rotor.TargetVelocityRPM > 0) {
            appendToDisplay("invert velocity");
            rotor.TargetVelocityRPM = rotor.TargetVelocityRPM * -1.0f;
        }
    } else if (rotor.UpperLimitDeg >= config.max || rotor.UpperLimitDeg == float.MaxValue) {
        appendToDisplay("By Upper Limit");
        rotor.UpperLimitDeg = config.parking;
        if (rotor.TargetVelocityRPM < 0) {
            appendToDisplay("invert velocity");
            rotor.TargetVelocityRPM = rotor.TargetVelocityRPM * -1.0f;
        }
    } 
    rotor.RotorLock = false;
}

void initWorkingPosition(RotorConfig config) {
    appendToDisplay("Move to Working Position");
    IMyMotorAdvancedStator rotor = config.rotor;
    rotor.RotorLock = true;
    double rotorDeg = MathHelper.ToDegrees(rotor.Angle);
    appendToDisplay("rotoDeg: " + rotorDeg);
    if (rotorDeg <= config.min) {
        appendToDisplay("by upper limit");
        rotor.UpperLimitDeg = config.max;
        if (rotor.TargetVelocityRPM < 0) {
            appendToDisplay("invert velocity");
            rotor.TargetVelocityRPM = rotor.TargetVelocityRPM * -1.0f;
        }
    } else {
        appendToDisplay("by lower limit");
        rotor.LowerLimitDeg = config.min;
        if (rotor.TargetVelocityRPM > 0) {
            appendToDisplay("invert velocity");
            rotor.TargetVelocityRPM = rotor.TargetVelocityRPM * -1.0f;
        }
    }
    rotor.RotorLock = false;
}

bool parkingPositionReached(RotorConfig config) {
    IMyMotorAdvancedStator rotor = config.rotor;
    double rotorDeg = MathHelper.ToDegrees(rotor.Angle);
    return Math.Abs(rotorDeg - config.parking) <= 0.1f;
}

bool workingPositionReached(RotorConfig config) {
    IMyMotorAdvancedStator rotor = config.rotor;
    double rotorDeg = MathHelper.ToDegrees(rotor.Angle);
    double targetDeg = config.min;
    if (rotor.TargetVelocityRPM > 0) {
        targetDeg = config.max;
    }
    return Math.Abs(rotorDeg - targetDeg) <= 0.1f;
}

public Program() {
    //Display
    _outputPanelDisplay = Me.GetSurface(0);
    _outputPanelDisplay.ContentType = VRage.Game.GUI.TextPanel.ContentType.TEXT_AND_IMAGE;
    _outputPanelKeyboard = Me.GetSurface(1);
    _outputPanelKeyboard.ContentType = VRage.Game.GUI.TextPanel.ContentType.TEXT_AND_IMAGE;

    writeToDisplay("");

    MyIniParseResult result;
    if (!_ini.TryParse(Me.CustomData, out result)) 
        throw new Exception(result.ToString());

    _rotorTop = initRotor("RotorTop");
    _rotorSide = initRotor("RotorSide");

}
//public void Save() {}

bool checkRotorIfContinue(RotorConfig config, bool parkingIsSet) {
    bool result = true;
    if (parkingIsSet && parkingPositionReached(config)) {
        config.rotor.RotorLock = true;
        config.rotor.UpperLimitDeg = config.parking;
        config.rotor.LowerLimitDeg = config.parking;
        config.rotor.RotorLock = false;
        showOffline();
        writeToDisplay("Parkposition reached");
        result = false;
    } else if (!parkingIsSet && workingPositionReached(config)) {
        config.rotor.RotorLock = true;
        config.rotor.LowerLimitDeg = config.min;
        config.rotor.UpperLimitDeg = config.max;
        config.rotor.RotorLock = false;
        showOffline();
        writeToDisplay("Workposition reached");
        result = false;
    }
    return result;
}
public void Main(string argument, UpdateType updateSource) {

    if (_rotorTop.rotor == null || _rotorTop.rotor == null) {
        showOffline();
        appendToDisplay("Top or side rotor missing!!!!");
        return;
    }

    if (updateSource == UpdateType.Terminal || updateSource == UpdateType.Trigger) {
        writeToDisplay("");
        showOnline();
        bool parkingIsSet = false;
        if (_commandLine.TryParse(argument)) {
            parkingIsSet = _commandLine.Switch("parking");
        }
        
        if (parkingIsSet) {
            initParkingPosition(_rotorTop);
            initParkingPosition(_rotorSide);
        } else {
            initWorkingPosition(_rotorTop);
            initWorkingPosition(_rotorSide);
        }
        Runtime.UpdateFrequency = UpdateFrequency.Update10;
    } else if (updateSource == UpdateType.Once || updateSource == UpdateType.Update10) {
        bool parkingIsSet = _commandLine.Switch("parking");
        // Lazy evaulation no problem because it just checks the end condition, so if top rotor reached it postion the other defines the state
        if (checkRotorIfContinue(_rotorTop, parkingIsSet) || checkRotorIfContinue(_rotorSide, parkingIsSet)) {
            Runtime.UpdateFrequency = UpdateFrequency.Update10;
        } else {
            Runtime.UpdateFrequency = UpdateFrequency.None;
        }
    }
}



#region PreludeFooter
    }
}
#endregion
