#region Prelude
using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game.ModAPI.Ingame.Utilities;

// Change this namespace for each script you create.
namespace SpaceEngineers.UWBlockPrograms.Cargo.DroneControl {
    public sealed class Program : MyGridProgram {
    // Your code goes between the next #endregion and #region
#endregion

const String messageTag =  "DroneControl";

const String scriptTypeController = "DCController";
const String scriptTypeDrone = "DCDrone";

const String configSectionGeneral = "General";
const String configNameType = "scriptType";
const String configNameConnectors = "connectors";

const int idKeyboard = 1;
const int idDisplay = 0;
MyIni _ini = new MyIni();
IMyProgrammableBlock progBlock;

int counter = 0;
String currentType;
IMyBroadcastListener bCastListener;

List<IMyShipConnector> connectors = new List<IMyShipConnector>();

public void writeTemplate() {
    _ini.Clear();
    _ini.Set(configSectionGeneral, configNameType, scriptTypeController);
    Me.CustomData = _ini.ToString();
}

public void appendTo(int id, String text) {
    // Display hat 11 Zeilen in Defaultfont
    IMyTextSurface currSurface = progBlock.GetSurface(id);
    currSurface.ContentType = VRage.Game.GUI.TextPanel.ContentType.TEXT_AND_IMAGE;
    currSurface.WriteText(currSurface.GetText() + "\n" + text);
}

public void writeTo(int id, String text) {
    IMyTextSurface currSurface = progBlock.GetSurface(id);
    currSurface.ContentType = VRage.Game.GUI.TextPanel.ContentType.TEXT_AND_IMAGE;
    currSurface.WriteText(text);
}

public Program() {
    counter = 0;
    progBlock = (IMyProgrammableBlock) GridTerminalSystem.GetBlockWithName(Me.CustomName);
    MyIniParseResult result;
    if (!_ini.TryParse(Me.CustomData, out result)) 
        throw new Exception(result.ToString());

    currentType = _ini.Get(configSectionGeneral, configNameType).ToString();
    if (currentType.Equals(scriptTypeController)) {
        String[] connectorNames = _ini.Get(configSectionGeneral, configNameConnectors).ToString().Split('\n');
        foreach (String name in connectorNames)
        {
            connectors.Add((IMyShipConnector) GridTerminalSystem.GetBlockWithName(name));
        } 
        
    }
    
    
    // bCastListener = IGC.RegisterBroadcastListener(messageTag);
    //     bCastListener.SetMessageCallback(messageTag);
    // if (currentType.Equals(scriptTypeReceiver)) {
        
    // } else {
    //     Runtime.UpdateFrequency |= UpdateFrequency.Once;
    // }
}
public void Save() {

}

public void Main(string argument, UpdateType updateSource) {

    writeTo(idDisplay, "I am " + currentType);

    if (currentType.Equals(scriptTypeController)) {
        if (updateSource == UpdateType.Terminal) {
            foreach (IMyShipConnector connector in connectors) {
                appendTo(idDisplay, connector.DisplayNameText + " locked: " + connector.Status);
                appendTo(idDisplay, "GPS:"+connector.DisplayNameText+":" + connector.GetPosition().X+":"+ connector.GetPosition().Y+":"+ connector.GetPosition().Z+":");
            }
        }
    }

    

// const int idDisplay = 0;
// IMyProgrammableBlock progBlock = (IMyProgrammableBlock) GridTerminalSystem.GetBlockWithName(Me.CustomName);
// IMyTextSurface currSurface = progBlock.GetSurface(idDisplay);
// currSurface.ContentType = VRage.Game.GUI.TextPanel.ContentType.TEXT_AND_IMAGE;
// IMyTerminalBlock block = GridTerminalSystem.GetBlockWithName("IceT Remote Control");
// currSurface.WriteText ("GPS:"+block.DisplayNameText+":" + block.GetPosition().X+":"+ block.GetPosition().Y+":"+ block.GetPosition().Z+":");

    // if (currentType.Equals(scriptTypeSender) && updateSource == UpdateType.IGC) {
    //     appendTo(idDisplay, updateSource + ": sending " + counter);
    //     IGC.SendBroadcastMessage(messageTag, "testsend " + counter++); 
    // } else if (currentType.Equals(scriptTypeReceiver) && updateSource == UpdateType.IGC) {
    //    MyIGCMessage message = bCastListener.AcceptMessage();
    //    appendTo(idDisplay, message.Source + " -> " + message.As<String>());
    //    IGC.SendUnicastMessage(message.Source, messageTag, "ack");
    // } else {
    //     appendTo(idDisplay, "pending " + bCastListener.HasPendingMessage);
        
    // }
    // appendTo(idDisplay, "updateSource " + updateSource);

    

}

#region PreludeFooter
    }
}
#endregion

