#region Prelude
using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game.ModAPI.Ingame.Utilities;

// Change this namespace for each script you create.
namespace SpaceEngineers.UWBlockPrograms.FillLight {
    public sealed class Program : MyGridProgram {
    // Your code goes between the next #endregion and #region
#endregion

List<IMyCargoContainer> containerList = new List<IMyCargoContainer>();
String lightName = "name";
IMyLightingBlock lightingBlock;
public Program() {
    GridTerminalSystem.GetBlocksOfType(containerList);
    if (containerList.Count > 0) {
        Echo("Found " + containerList.Count + " containers.");
        Runtime.UpdateFrequency = UpdateFrequency.Update100;
    } else {
        throw new Exception("No container found");
    }
    lightingBlock = (IMyLightingBlock) GridTerminalSystem.GetBlockWithName(lightName);
    if (lightingBlock == null) {
        throw new Exception("Light not found");
    }
}
//public void Save() {}

public void Main(string argument, UpdateType updateSource) {
    bool allFull = true;
    bool allEmpty = false;
    foreach (IMyCargoContainer container in containerList) {
        allEmpty |= container.GetInventory().ItemCount > 0;
        allFull &= container.GetInventory().IsFull;
    }
    Color lightColor;
    if (allFull) {
        lightColor = Color.Red;
    } else if (allEmpty) {
        lightColor = Color.Green;
    } else {
        lightColor = Color.Yellow;
    }
    lightingBlock.Color = lightColor;
}



#region PreludeFooter
    }
}
#endregion
